# Gitlab Pipline templates

This project contains a file with all templates and needed variables.

## Usage
Choose a job to include in security_scan stage in order to successfully reach the desired result.

```yaml
stages:
- security_scan
```
Add needed variables and files inside the Repo and inside the CICD variables in gitlab.
Create .gitlab-ci.yml file and add to the project with the desired stages included.
### Examples:

```yaml
## Security Scan

### Dotnet 6.0 Security Scan
#### Variables:
    # SONAR_TOKEN : SonarQube token
    # SONAR_PROJECT_KEY : SonarQube project key generated per project
    # INTERNAL_ARTIFACTORY_REPO
    # INTERNAL_ARTIFACTORY_USERNAME
    # INTERNAL_ARTIFACTORY_PASSWORD
    # PROJECT
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/scanners'
    ref: master
    file: '/sonarqube-dotnet-scanner.yml'
### Golang Security Scan
#### Variables:
    # SONAR_PROJECT_KEY : SonarQube project key generated per project
    # SONAR_TOKEN : SonarQube token
    # EXCLUSIONS  : list of files to exclude from analysis
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/scanners'
    ref: master
    file: '/sonarqube-go-scanner.yml'
### Java Maven Security Scan
#### Variables:
    # SONAR_PROJECT_KEY : SonarQube project key generated per project
    # SONAR_TOKEN : SonarQube token
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/scanners'
    ref: master
    file: '/sonarqube-maven-scanner.yml'
### Nodejs Security Scan
#### Variables:
    # SONAR_PROJECT_KEY : SonarQube project key generated per project
    # SONAR_TOKEN : SonarQube token
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/scanners'
    ref: master
    file: '/sonarqube-nodejs-scanner.yml'   
### general Security Scan
#### Variables:
    # SONAR_PROJECT_KEY : SonarQube project key generated per project
    # SONAR_TOKEN : SonarQube token
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/scanners'
    ref: master
    file: '/sonarqube_general_scanner.yml'

```